/*
    Plugin Name: Simple Video Player (svPlayer)
	Plugin URI: 
	Description: Simple implementation of a HTML5 video player for WordPress
	Version: 1.6
	Author: ThemeSpartans
	Author URI: http://codecanyon.net/user/themeSpartans
	
	browsers supported: IE10 and later,Mozilla,Safari,Chrome,Opera IOS!	
*/
jQuery.noConflict();
(function( $ ) {
	//create plug in name function!
	$.fn.svPlayer=function(options){
		
		var	container=$(this);
		/*Null Variables*/
		var videoPlayer;
		var positionSet;
		var mouseX;
		var enterFrame;
		var widthdiff;
		var scrollPos;
		var currentText;
		var controlsTimer;
		/*Boolean Variables*/
		var seekEnabled=false;
		var isPlay=false;
		var replayVideos=false;
		var showFullScreen=false;
		/*Number Variables*/
		var currentPlay= 0;
		var currentSeek= 0;		
		var startYvar = 0;
		var currWidth = 0;
		var thisWidth = 0;
		var newVar = 0;
		var newPosition= 0;
		var speed= 0;
		var video_height=0;	
		var video_width=0;
		/*Array Variables*/
		var xmlArray=[];
		var videosArray=[];		
		var videoItems=[];
		
		/*Svg viariables*/
		var playPause_svg_default='<path fill="#ED1C24" d="M8.3,4.9l9.9,6.5c0.8,0.5,0.8,1.7,0,2.2l-9.9,6.5C7.3,20.7,6.2,20,6.2,19V6C6.2,5,7.3,4.3,8.3,4.9z"/>';
		var playPause_svg_selected='<path fill="#ED1C24" d="M9.4,4.7H8.1c-1,0-1.8,0.8-1.8,1.9v11.9c0,1,0.8,1.9,1.9,1.9h1.3c1,0,1.9-0.8,1.9-1.9V6.6C11.3,5.5,10.5,4.7,9.4,4.7z"/>'+
		'<path fill="#ED1C24" d="M16.8,4.7h-1.3c-1,0-1.9,0.8-1.9,1.9v11.9c0,1,0.8,1.9,1.9,1.9h1.3c1,0,1.9-0.8,1.9-1.9V6.6C18.7,5.5,17.9,4.7,16.8,4.7z"/>';
		var speaker_svg_default='<path fill="#ED1C24" d="M13.3,4.9L10.1,7C10,7,9.8,7,9.7,7H5.1c-1.8,0-3.2,1.4-3.2,3.2v4.6c0,1.8,1.4,3.2,3.2,3.2h4.6c0.1,0,0.3,0,0.4,0l3.2,2.1c0.9,0.6,2.1-0.1,2.1-1.1V6C15.4,4.9,14.2,4.3,13.3,4.9z"/>'+
		'<path fill="#ED1C24" d="M17.6,15.7c1.3-0.5,2.2-1.8,2.2-3.2c0-1.5-0.9-2.7-2.2-3.2C17,9,16.3,9.6,16.3,10.2l0,0c0,0.4,0.2,0.8,0.6,0.9c0.5,0.2,0.9,0.8,0.9,1.4c0,0.6-0.4,1.2-0.9,1.4c-0.4,0.1-0.6,0.5-0.6,0.9l0,0C16.3,15.4,17,15.9,17.6,15.7z"/>'+
		'<path fill="#ED1C24" d="M17.4,5.5c-0.6-0.1-1.1,0.4-1.1,1c0,0.5,0.4,0.9,0.9,1c2.4,0.4,4.1,2.5,4.1,4.9c0,2.5-1.8,4.5-4.1,4.9c-0.5,0.1-0.9,0.5-0.9,1c0,0.6,0.5,1.1,1.1,1c3.3-0.5,5.9-3.4,5.9-6.9S20.7,6.1,17.4,5.5z"/>';
		var speaker_svg_selected='<path fill="#ED1C24" d="M12.8,4.8L9.6,6.9c-0.1,0-0.3,0-0.4,0H4.6c-1.8,0-3.2,1.4-3.2,3.2v4.6c0,1.8,1.4,3.2,3.2,3.2h4.6c0.1,0,0.3,0,0.4,0l3.2,2.1c0.9,0.6,2.1-0.1,2.1-1.1v-13C14.8,4.9,13.7,4.3,12.8,4.8z"/>'+
		'<path fill="#ED1C24" d="M23.4,15.4l-1.8-2.5c-0.3-0.4-0.3-1,0-1.4l1.4-2c0.6-0.8,0-2-1-2c-0.4,0-0.8,0.2-1,0.5l-0.4,0.6c-0.5,0.7-1.6,0.7-2.1,0L18.2,8c-0.2-0.4-0.6-0.6-1-0.6H17c-1,0-1.6,1.1-1,2l1.5,2.1c0.3,0.4,0.3,1,0,1.4l-1.7,2.4c-0.6,0.8,0,1.9,1,1.9l0,0c0.4,0,0.8-0.2,1-0.6l0.6-0.9c0.5-0.7,1.6-0.7,2.1,0l0.6,0.9c0.2,0.3,0.6,0.6,1,0.6h0.2C23.4,17.3,24,16.2,23.4,15.4z"/>';
		var fullscreen_svg_default='<path fill="#ED1C24" d="M14.5,17.1l-6.6-6.6c-0.8-0.8-2.3-0.4-2.4,0.9l-0.7,7.3c-0.1,0.9,0.6,1.6,1.5,1.5l7.3-0.7C14.8,19.4,15.3,18,14.5,17.1z"/>'+
		'<path fill="#ED1C24" d="M10.6,7.8l6.6,6.6c0.8,0.8,2.3,0.4,2.4-0.9l0.6-7.2c0.1-0.9-0.6-1.6-1.5-1.5l-7.2,0.6C10.3,5.4,9.8,6.9,10.6,7.8z"/>';
		var fullscreen_svg_selected='<path fill="#ED1C24" d="M13.1,3.7L12.4,11c-0.1,0.9,0.6,1.6,1.5,1.5l7.3-0.7c1.2-0.2,1.7-1.6,0.9-2.4l-6.6-6.6'+
		'C14.8,2,13.2,2.5,13.1,3.7z"/>'+
		'<path fill="#ED1C24" d="M11,12.5l-7.2,0.6c-1.2,0.1-1.7,1.5-0.9,2.4l6.6,6.6c0.8,0.8,2.3,0.4,2.4-0.9l0.6-7.2'+
		'C12.6,13.1,11.9,12.4,11,12.5z"/>';
		var twitter_svg='<path fill="#FF0000" d="M12.5,2.5c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S18,2.5,12.5,2.5z M19.6,8.2'+
		'c-0.1,0.6-0.9,1-1.3,1.5c0.4,6.8-7.2,10.9-12.7,7.5c1.6,0,3.2-0.4,4.3-1.3c-1.3-0.1-2.4-0.7-2.8-1.9c0.4-0.1,1,0.1,1.2-0.1'+
		'C7.1,13.4,6,12.7,6,11c0.4,0.1,0.7,0.3,1.2,0.3c-0.7-0.7-1.6-2.7-0.9-4c1.5,1.6,3.2,2.8,6,3.1c-0.7-2.9,3.2-4.9,5-2.7'+
		'c0.7-0.1,1.3-0.4,1.9-0.7c-0.3,0.7-0.7,1.2-1.2,1.6C18.6,8.5,19.1,8.4,19.6,8.2z"/>';
		var facebook_svg='<path fill="#ED2224" d="M12.5,2.5c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S18,2.5,12.5,2.5z M16.5,7.9c0,0-0.7,0-1.2,0'+
		'c-1.2,0-1.1,0.7-1.1,1.6h1.9v3.1h-1.9v7.8h-4.1v-7.8H8.5v-3H10V9.1c0-0.4,0-0.8,0.1-1.5c0.1-0.6,0.3-0.8,0.6-1.3'+
		'C11,6,11.9,4.5,14.4,4.5c0.7,0,2.1,0,2.1,0V7.9z"/>';
		var google_svg='<path fill="#FF0000" d="M9.9,11.2c0.4,0,0.7-0.1,0.9-0.4C11,10.5,11,10,11,9.3c0-0.6-0.1-1.2-0.3-1.5c-0.1-0.4-0.6-0.6-0.9-0.6'+
		'c-0.3,0-0.4,0.1-0.6,0.1C9.1,7.5,9,7.7,8.9,7.8c0,0.3-0.1,0.4-0.1,0.7S8.6,9,8.6,9.3c0,0.6,0.1,1,0.3,1.5C9.1,11,9.4,11.2,9.9,11.2'+
		'z"/>'+
		'<path fill="#FF0000" d="M11.5,15.1c-0.1,0-0.4-0.1-0.9-0.1c-0.4,0-1,0-1.7,0c-0.3,0.1-0.4,0.3-0.6,0.6c-0.1,0.3-0.1,0.4-0.1,0.7'+
		'c0,0.1,0,0.3,0.1,0.4s0.1,0.3,0.3,0.4c0.1,0.1,0.4,0.4,0.6,0.4c0.3,0.1,0.6,0.1,1,0.1c0.7,0,1.2-0.1,1.6-0.4'+
		'c0.4-0.3,0.6-0.7,0.6-1.3c0-0.1-0.1-0.4-0.1-0.6C12.1,15.3,11.9,15.1,11.5,15.1z"/>'+
		'<path fill="#FF0000" d="M12.5,2.5c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S18,2.5,12.5,2.5z M14.4,16.6'+
		'c-0.1,0.4-0.4,0.6-0.9,0.9c-0.4,0.3-0.9,0.4-1.5,0.6c-0.6,0.1-1.3,0.3-2.2,0.3c-1.5,0-2.5-0.1-3.2-0.4c-0.7-0.3-1.2-0.9-1.2-1.6'+
		'c0-0.4,0.3-0.7,0.6-1c0.3-0.3,1-0.4,1.8-0.4v0c-0.3-0.1-0.7-0.4-1-0.7c-0.1-0.1-0.4-0.6-0.4-0.9c0-0.3,0.1-0.4,0.1-0.6'+
		'c0.1-0.1,0.3-0.3,0.4-0.4c0.1-0.1,0.4-0.3,0.6-0.3c0.3,0,0.4-0.1,0.6-0.1l0,0c-0.7-0.3-1.3-0.7-1.6-1c-0.3-0.4-0.6-0.9-0.6-1.5'+
		'c0-0.7,0.4-1.5,1.2-1.9C7.8,7,8.7,6.7,9.9,6.7c0.4,0,0.7,0,1,0.1C11.2,7,11.5,7,11.6,7h3.1v0.9h-1.8c0.3,0.3,0.4,0.4,0.4,0.7'+
		'c0.1,0.3,0.1,0.6,0.1,0.7c0,0.4-0.1,0.7-0.3,1c-0.3,0.3-0.4,0.6-0.7,0.7c-0.3,0.3-0.7,0.4-1.2,0.4c-0.4,0.1-1,0.3-1.5,0.3H8.7'+
		'c-0.1,0-0.3,0.1-0.3,0.1c-0.1,0.1-0.1,0.3-0.1,0.3c0,0.3,0.1,0.4,0.3,0.4c0.1,0.1,0.6,0.1,1,0.1c0.4,0,0.9,0,1.5,0.1'+
		'c0.4,0,0.9,0.1,1.2,0.1c0.3,0,0.6,0.1,0.9,0.1c0.4,0,0.7,0.1,0.9,0.4s0.4,0.4,0.6,0.7c0.1,0.3,0.3,0.6,0.3,1'+
		'C14.7,15.9,14.7,16.3,14.4,16.6z M17.6,10v1.9H16V10h-1.9V8.4H16V6.7h1.6v1.9h1.9v1.6h-1.9V10z"/>';
		var list_svg='<path fill="#ED1C24" d="M3.9,6.3L3.9,6.3c0,1,0.8,1.8,1.7,1.8h13.8c0.9,0,1.7-0.8,1.7-1.7V6.3c0-0.9-0.8-1.7-1.7-1.7H5.5'+
		'C4.6,4.6,3.9,5.4,3.9,6.3z"/>'+
		'<path fill="#ED1C24" d="M5.5,14.2h13.8c0.9,0,1.7-0.8,1.7-1.7v-0.1c0-0.9-0.8-1.7-1.7-1.7H5.5c-0.9,0-1.7,0.8-1.7,1.7v0.1'+
		'C3.9,13.4,4.6,14.2,5.5,14.2z"/>'+
		'<path fill="#ED1C24" d="M5.5,20.3h13.8c0.9,0,1.7-0.8,1.7-1.7v-0.1c0-0.9-0.8-1.7-1.7-1.7H5.5c-0.9,0-1.7,0.8-1.7,1.7v0.1'+
		'C3.9,19.4,4.6,20.3,5.5,20.3z"/>';
		var replay_svg='<path fill="#FF0000" d="M13.2,4.7C11,4.7,9,5.6,7.5,7.2L6,6.1c-0.6-0.5-1.5,0-1.4,0.8l0.7,5.2c0.1,0.5,0.5,0.8,1,0.7l5.2-0.7'+
		'c0.8-0.1,1-1.1,0.4-1.5l-1.4-1c0.7-0.6,1.7-1,2.7-1c2.2,0,4,1.8,4,4s-1.8,4-4,4c-1.3,0-2.5-0.6-3.2-1.6c-0.6-0.8-1.8-1-2.7-0.4'+
		'c-0.8,0.6-1,1.8-0.4,2.7c1.5,2,3.8,3.2,6.3,3.2c4.3,0,7.8-3.5,7.8-7.8S17.5,4.7,13.2,4.7z"/>';
		var social_svg='<path fill="#FF0000" d="M151.5,123.6c-5.1,0-9.8,1.8-13.5,4.7l-60.3-26.2l60.3-26.2c3.7,3,8.4,4.7,13.5,4.7'+
		'c11.9,0,21.5-9.6,21.5-21.5c0-11.9-9.6-21.5-21.5-21.5c-11.3,0-20.6,8.8-21.4,19.9l-64,27.8c-3.7-3-8.4-4.7-13.5-4.7'+
		'c-11.9,0-21.5,9.6-21.5,21.5c0,11.9,9.6,21.5,21.5,21.5c5.1,0,9.8-1.8,13.5-4.7l64,27.8c0.8,11.1,10.1,19.9,21.4,19.9'+
		'c11.9,0,21.5-9.6,21.5-21.5C173,133.2,163.4,123.6,151.5,123.6z"/>';

		/*Seetings for the player*/
		var settings = $.extend({
			
			title:'',
			poster:'',
			videos:[],
			autoStart: true,
			videoWidth:"default",
			setMutedvideoBg: true,
			autoReplay: false,
			showControls: true,
			preload: "auto"
			
		}, options );
		
		//Function init start the player plugin
		function init(){
			buildPlayer();					
			//initializaPlayer();
		}
		
		//function build player layout Fallback message if not support html5 browser!
		function buildPlayer(){			
				container.append('<div class="svplayer_preloader"><div class="svplayer_dot svplayer_dot1"></div><div class="svplayer_dot svplayer_dot2"></div><div class="svplayer_dot svplayer_dot3"></div></div>');
				container.append('<div id="svplayer"></div>');
				if(jQuery.browser.mobile){
					if(settings.videoWidth!="full"){
						container.find("#svplayer").append('<div id="sv_header"><div>')
						.append('<div id="sv_controls"></div>')
						.append('<div id="sv_videoWrap"></div>')									
						.append('<div id="sv_videoTitle"></div>');
						
						container.find("#sv_header").append('<div id="social_button"></div>')
						.append('<div id=open></div>');
						container.find("#sv_header #open").append('<div id="shareTwitter"></div>')
						.append('<div id="shareFacebook"></div>')
						.append('<div id="shareGoogle"></div>');
						container.find("#sv_controls").append('<div id="list"></div>')
						.append('<div id=open></div>');
						container.find("#sv_controls #open").append('<div id="plasyPause"></div>')	
						.append('<div id="replay"></div>');						
					}
				}else{
					container.find("#svplayer").append('<div id="sv_videoWrap"></div>')
					.append('<div id="knobSlider_svplayer"><input class="knob" data-fgColor="#cc181e" data-bgColor="#777777" data-displayInput="false" data-thickness=0.15 value="0" data-width="200"><div id="playPauseMain"></div></div>')
					.append('<div id="sv_header"><div>')
					.append('<div id="sv_controls"></div>');
							
					container.find("#sv_header").append('<div id="social_button"></div>')
					.append('<div id=open></div>');
					container.find("#sv_header #open").append('<div id="sv_videoTitle"></div>')
					.append('<div id="shareTwitter"></div>')
					.append('<div id="shareFacebook"></div>')
					.append('<div id="shareGoogle"></div>');
					container.find("#sv_controls").append('<div id="list"></div>')
					.append('<div id=open></div>');
					container.find("#sv_controls #open").append('<div id="plasyPause"></div>')				
					.append('<div id="soundSpaker"></div>')
					.append('<div id="fullscreen"></div>')				
					.append('<div id="replay"></div>');	
				}
				container.find("#social_button").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 205 205" enable-background="new 0 0 205 205" xml:space="preserve"><g>'+social_svg+'</g></svg>');
				container.find("#shareTwitter").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+twitter_svg+'</g></svg>');
				container.find("#shareGoogle").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+google_svg+'</g></svg>');
				container.find("#shareFacebook").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">'+facebook_svg+'</svg>');
				container.find("#plasyPause").append('<div id="default"><div>')
				.append('<div id="selected"></div>');
				container.find("#playPauseMain").append('<div id="default"><div>')
				.append('<div id="selected"></div>')
				.append('<div id="timerbar"></div>');
				container.find("#plasyPause #default").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">'+playPause_svg_default+'</svg>');
				container.find("#plasyPause #selected").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+playPause_svg_selected+'</g></svg>');
				container.find("#playPauseMain #default").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">'+playPause_svg_default+'</svg>');
				container.find("#playPauseMain #selected").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+playPause_svg_selected+'</g></svg>');
				container.find("#soundSpaker").append('<div id="default"><div>')
				.append('<div id="selected"></div>');
				container.find("#soundSpaker #default").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+speaker_svg_default+'</g></svg>');
				container.find("#soundSpaker #selected").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+speaker_svg_selected+'</g></svg>');
				container.find("#list").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+list_svg+'</g></svg>');
				container.find("#replay").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">'+replay_svg+'</svg>');
				container.find("#fullscreen").append('<div id="default"><div>')
				.append('<div id="selected"></div>');
				container.find("#fullscreen #default").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+fullscreen_svg_default+'</g></svg>');
				container.find("#fullscreen #selected").append('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve"><g>'+fullscreen_svg_selected+'</g></svg>');
				container.find(".knob").knob({
						width: 200,
						height: 200,
						change : function(e){seekTrack();},
						released : function(e){seekStop();},
						release : function(e){seekStop();},
						clicked : function(e){seekStart();},
						hit : function(){seekStart();seekTrack();seekStop();}
				});
				container.find("#sv_videoWrap").append('<video id="video" preload="'+settings.preload+'">You browser not support html5 video!</video>');
								
				videoPlayer=container.find("#video")[0];
					
				if(!jQuery.browser.mobile){
					if(settings.showControls==false || settings.showControls=="false"){
						container.find("#sv_controls, #sv_header, #knobSlider_svplayer").css({"visibility":"hidden"});
					}
				}				
				if(settings.autoReplay){
					replayVideos=true;
					container.find("#replay").addClass("clicked");
				}
				if(settings.autoStart){
					container.find("#video").attr("autoplay","autoplay");
				}
				initializaPlayer();				
		}
		/*Initialize the player*/
		function initializaPlayer(){
			createvideos();	
			resetPlayer();
			createEvents();						
			videoPlayer.volume=1;	
		}
		/*Create the video load from settings*/
		function createvideos(){
			if(jQuery.browser.mobile){
				if(settings.videoWidth=="full"){
					$("body").css({"background-image":"url("+settings.poster+")","background-repeat":"no-repeat","background-position":"center","background-size":"cover","min-height":"100vh"});
					container.find("#svplayer").remove();
				}else{
					createMobile();
				}
			}
			var videoSources='';
			if(settings.title!=''){	
				container.find("#sv_videoTitle").html(settings.title);
			}else{
				container.find("#sv_videoTitle").html('Unknown video title!');
			}
			if(settings.poster!=''){	
				videoPlayer.setAttribute("poster",settings.poster);
			}
			for(var i=0; i<settings.videos.length; i++){
				if(settings.videos[i]!=''){
					videoSources+='<source src="'+settings.videos[i]+'" type="video/'+settings.videos[i].replace(/^.*\./, '')+'">';
				}
			}
			container.find("#video").html(videoSources);
			videoPlayer.load();
		}	
		/*Set the videos width from settings*/
		function setVideoStyles(){
			switch(settings.videoWidth){
				case "default":
					video_width=videoPlayer.videoWidth;
					video_height=videoPlayer.videoHeight;
					container.find("#svplayer").css({"max-width":video_width+"px","max-height":video_height+"px"});
					break;
				case "":
					video_width=videoPlayer.videoWidth;
					video_height=videoPlayer.videoHeight;
					container.find("#svplayer").css({"max-width":video_width+"px","max-height":video_height+"px"});
					break;
				case "full":
					backgroundVideo();
					break;
				default:
					video_width=settings.videoWidth;
					video_height=settings.videoWidth/(16/9);
					container.find("#svplayer").css({"max-width":video_width+"px","max-height":video_height+"px"});
					break;
			}
		}	
		//Function create mobile view!
		function createMobile(){
			container.find("#sv_videoTitle").css({"display":"block","padding":"0px","text-align":"center"});
			container.find("#sv_controls").css({"opacity":1,"position":"relative","float":"left"});
			container.find("#sv_header").css({"opacity":1,"position":"relative","float":"right"});	
			container.find("#sv_videoWrap").css({"display":"block","position":"relative","z-index":0,"height":"auto","width":"100%"});
			container.find("#sv_videoWrap video").css({"height":"auto","width":"100%","display":"block","z-index":0});			
			container.find("#svplayer").css({"opacity":0,"width":"100%","height":"auto","background-color":"#000000"});			
		}
		//create the events manage the player
		function createEvents(){
			//begin loading
			videoPlayer.addEventListener('loadstart',function (){
				resetPlayer();				
			},false);
			//first frame load complete	
			videoPlayer.addEventListener('loadedmetadata',function (){
				container.find("#svplayer").animate({"opacity": 1},1000);
				container.find(".svplayer_preloader").fadeOut(50);
				setVideoStyles();				
				if(!jQuery.browser.mobile){
					animateHover();		
				}			
			},false);					
			
			videoPlayer.addEventListener('timeupdate',updateRunTime,false);
			videoPlayer.addEventListener('play',playToPause,false);
			videoPlayer.addEventListener('pause',pauseToPlay,false);
			videoPlayer.addEventListener('ended',onEndPlay,false);	
				
			//Return message when wait for buffering!
			videoPlayer.addEventListener('waiting',function (){
				container.find("#timerbar").html('Buffering...');	
			},false);
			
			//Return erros on video play!  
			container.find("source").on("error",errorSound);		
			//buttons events
			
			container.find("#replay").bind("click",replayVids);			
			container.find("#plasyPause,#playPauseMain").bind("click",playPauseVideo);
			container.find("#fullscreen").click(function(e) {
			   fullScreenMode(container.find("#svplayer")[0]);
            });
			container.find("#soundSpaker").click(function(e) {
                muteUnmuted();
            });
			container.find("#shareFacebook").click(function(e) {
				var share_url=window.location.href;
                var url='https://www.facebook.com/sharer/sharer.php?u='+share_url+'&t='+container.find("#sv_videoTitle").text();
				window.open(url, '_blank');
            });
			container.find("#shareGoogle").click(function(e) {
				var share_url=window.location.href;
				var url='https://plus.google.com/share?url='+share_url;
                window.open(url, '_blank');
            });
			container.find("#shareTwitter").click(function(e) {
				var share_url=window.location.href;
                var url='https://twitter.com/share?url='+share_url;
				 window.open(url, '_blank');
            });			
			if(!jQuery.browser.mobile){
				setInterval(function(){
					repositionKnob();
				},10);		
			
				container.find("#sv_header,#sv_controls").mouseenter(function(e) {
					$(this).find("#open").stop().animate({width:'toggle'},500);
				});
				container.find("#sv_header,#sv_controls").mouseleave(function(e) {
					$(this).find("#open").stop().animate({width:'toggle'},500);
				});
			}else{
				container.find("#sv_header").click(function(e) {
					$(this).find("#open").stop().animate({width:'toggle'},500);
				});
				container.find("#sv_controls").click(function(e) {
					$(this).find("#open").stop().animate({width:'toggle'},500);
				});
			}
		}
		//Function reset player
		function resetPlayer(){
			container.find("#timerbar").html('00:00-00:00');
			container.find(".knob").val(0).trigger('change');
			pauseToPlay();
		}
		//Function return error sound!
		function errorSound(){
			container.find("#sv_videoTitle").append('-This video not found!');
		}
		//responsive change size knob slider!
		function repositionKnob(){
			if(container.find("#sv_videoWrap").height()<250){
				container.find("#knobSlider_svplayer").css({"width":"100px","height":"100px"});
				container.find(".knob").trigger('configure', {width:100,height:100});
				container.find("#playPauseMain").css({"width":"50px","height":"50px","margin-left":"-25px","margin-top":"-25px"});
				container.find("#playPauseMain svg").css({"width":"50px","height":"50px"});
				container.find("#timerbar").css({"font-size":"8px","width":"50px"});
			}else{
				container.find("#knobSlider_svplayer").css({"width":"200px","height":"200px"});
				container.find(".knob").trigger('configure', {width:200,height:200});
				container.find("#playPauseMain").css({"width":"100px","height":"100px","margin-left":"-50px","margin-top":"-50px"});
				container.find("#playPauseMain svg").css({"width":"100px","height":"100px"});
				container.find("#timerbar").css({"font-size":"14px","width":"100px"});
			}
			container.find("#knobSlider_svplayer").css({"left":(container.find("#sv_videoWrap").width()-container.find("#knobSlider_svplayer").innerWidth())/2+"px","top":(container.find("#sv_videoWrap").height()-container.find("#knobSlider_svplayer").innerHeight())/2+"px"});													
		}
		/*Function when end the video track*/
		function onEndPlay(){			
		  var oldPlay;
		  if(replayVideos!=false){			
				videoPlayer.play();
		  }				
		}
		/*Function replay or not videos*/
		function replayVids(){
			if(replayVideos!=false){
				replayVideos=false;
				$(this).removeClass("clicked");
			}else{
				replayVideos=true;
				$(this).addClass("clicked");
			}
		}
		//function play or pause track when button clicked
		function playPauseVideo(){
			if(videoPlayer.paused){
				videoPlayer.play();
			}else if(videoPlayer.played){
				videoPlayer.pause();
			}
		}
		//function manage the play to pause action when track stop playing
		function playToPause(){
			isPlay=true;
			container.find("#plasyPause #selected").css({"display":"block"});
			container.find("#plasyPause #default").css({"display":"none"});
			container.find("#playPauseMain #selected").css({"display":"block"});
			container.find("#playPauseMain #default").css({"display":"none"});
		}
		//function play the pause to play action when track start playing
		function pauseToPlay(){
			isPlay=false;
			container.find("#plasyPause #selected").css({"display":"none"});
			container.find("#plasyPause #default").css({"display":"block"});
			container.find("#playPauseMain #selected").css({"display":"none"});
			container.find("#playPauseMain #default").css({"display":"block"});
		}
		//function mute or unmute the sound when button clicked
		function muteUnmuted(){			
			if(videoPlayer.muted){
				videoPlayer.muted=false;
				container.find("#soundSpaker #selected").css({"display":"none"});
				container.find("#soundSpaker #default").css({"display":"block"});
			}else{
				videoPlayer.muted=true;
				container.find("#soundSpaker #selected").css({"display":"block"});
				container.find("#soundSpaker #default").css({"display":"none"});		
			}
		}
		//function update run time for the track play and seek bar show
		function updateRunTime(){
			if(!seekEnabled && isPlay){
				container.find("#timerbar").html(timerConvert(this.currentTime)+'-'+timerConvert(this.duration));	
				currentSeek=Math.floor((this.currentTime/this.duration)*100);			
				container.find(".knob").val(currentSeek).trigger('change');
			}
		}
		//function when start seek track bar
		function seekStart(){
			seekEnabled=true;
		}
		//function when stop seek track bar
		function seekStop(){
			seekEnabled=false;				
		}
		//function when bar seeked with mouse
		function seekTrack(){
			if(seekEnabled){		
				videoPlayer.currentTime=(container.find(".knob").val()/100)*videoPlayer.duration;
			}
		}
		//function convert track timers
		function timerConvert(setTimer){
			if(isFinite(setTimer)){
				var mins=0;
				var secs=0;
				mins=Math.floor(setTimer/60);
				mins = mins >= 10 ? mins : '0' + mins;
				secs=Math.floor(setTimer%60);
				secs = secs >= 10 ? secs : '0' + secs;
				return mins+':'+secs;
			}else{
				return '00:00';
			}
		}
		//Function animate buttons on mouse over the player
		function animateHover(){			
			container.find("#svplayer").bind("mouseover",function(e) {					
				showControls();
				clearTimeout(controlsTimer);
				controlsTimer=setTimeout(hideControls,5000);			
			}).bind("mouseout",function(e) {
				hideControls();	
				clearTimeout(controlsTimer);				
			});	
		}
		/*Function create full video background*/
		function backgroundVideo(){
			if(!jQuery.browser.mobile){
				videoPlayer.play();
				if(settings.setMutedvideoBg==true)
				{
					videoPlayer.volume=0;
				}
				else if(settings.setMutedvideoBg==false)
				{
					videoPlayer.volume=1;
				}
				container.find("#svplayer").css({"position":"fixed","z-index":-9999,"top":"0px","left":"0px","width":"100%","height":"100%"});			
				container.find("#sv_header,#sv_controls,#knobSlider_svplayer").css({"display":"none"});
				$("body").css({"margin":0});		
				resizeVideo();
				$(window).resize(function(e) {
					resizeVideo();
				});
			}
		}
		/*function resize video background keeping ratio*/
		function resizeVideo(){
			var ratio=container.find("video").height()/container.find("video").width();
			var screenRatio=$(window).height()/$(window).width();		
			if(screenRatio>ratio){
				container.find("#svplayer video").width($(window).height()/ratio);
				container.find("#svplayer video").css({"left":($(window).width()-container.find("#svplayer video").width())/2+"px","top":"0px"});
			}else{
				container.find("#svplayer video").width("100%");
				container.find("#svplayer video").css({"left":"0px","top":($(window).height()-container.find("#svplayer video").height())/2+"px"});
			}
		}
		/*function show sv_controls when mouse over video*/
		function showControls(){
			container.find("#sv_header").css({"opacity":1});
			container.find("#sv_controls").css({"opacity":1});
			container.find("#knobSlider_svplayer").css({"opacity":1});
		}
		/*Function hide sv_controls when mouse out the video*/
		function hideControls(){
			container.find("#sv_header").css({"opacity":0});
			container.find("#sv_controls").css({"opacity":0});
			container.find("#knobSlider_svplayer").css({"opacity":0});
		}
		/*Function set fullscreen or exit full screeen*/
		function fullScreenMode(elem) {

		  if (!document.fullscreenElement &&    // alternative standard method
			  !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (elem.requestFullscreen) {
			  elem.requestFullscreen();
			} else if (elem.msRequestFullscreen) {
			  elem.msRequestFullscreen();
			} else if (elem.mozRequestFullScreen) {
			  elem.mozRequestFullScreen();
			} else if (elem.webkitRequestFullscreen) {
			  elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		  } else {
			if (document.exitFullscreen) {
			  document.exitFullscreen();
			} else if (document.msExitFullscreen) {
			  document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
			  document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
			  document.webkitExitFullscreen();
			}
		 }
	  }
	  /*Function when fullscreen enabled or disabled*/
	  document.addEventListener("fullscreenchange", function () {
			if(document.fullScreen){
				container.find("#fullscreen #selected").css({'display':'block'});
				container.find("#fullscreen #default").css({'display':'none'});
			}else{
				container.find("#fullscreen #default").css({'display':'block'});
				container.find("#fullscreen #selected").css({'display':'none'});
			}
		}, false);
		 
		document.addEventListener("mozfullscreenchange", function () {
			if(document.mozFullScreen){
				container.find("#fullscreen #selected").css({'display':'block'});
				container.find("#fullscreen #default").css({'display':'none'});
			}else{
				container.find("#fullscreen #default").css({'display':'block'});
				container.find("#fullscreen #selected").css({'display':'none'});
			}
		}, false);
		 
		document.addEventListener("webkitfullscreenchange", function () {
			if(document.webkitIsFullScreen){
				container.find("#fullscreen #selected").css({'display':'block'});
				container.find("#fullscreen #default").css({'display':'none'});
			}else{
				container.find("#fullscreen #default").css({'display':'block'});
				container.find("#fullscreen #selected").css({'display':'none'});
			}
		}, false);
		document.addEventListener("msfullscreenchange", function () {
			if(document.msFullscreenElement){
				container.find("#fullscreen #selected").css({'display':'block'});
				container.find("#fullscreen #default").css({'display':'none'});
			}else{
				container.find("#fullscreen #default").css({'display':'block'});
				container.find("#fullscreen #selected").css({'display':'none'});
			}
		}, false);
		//starting player
	init();
	}
})( jQuery );
//end of jquery plugin!